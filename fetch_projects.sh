#!/bin/bash

# directory where reside this script (not working if no simlinks are in the path).
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECTS_DIR="$DIR/web"

# read config file passed as input
while IFS=' ' read -r name url ; do
        echo "[x] project '$name' resides at '$url'."
        if [ -s "$PROJECTS_DIR/$name" ]
            then
            echo "    Pulling repository."
            git -C "$PROJECTS_DIR/$name" pull
        else
            echo "    Cloning repository."
            git clone "$url" "$PROJECTS_DIR/$name"
        fi

done < "$1"
